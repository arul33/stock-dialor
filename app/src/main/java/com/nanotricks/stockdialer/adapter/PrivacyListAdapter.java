package com.nanotricks.stockdialer.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nanotricks.stockdialer.R;
import com.nanotricks.stockdialer.interfaces.ContactListLisenter;
import com.nanotricks.stockdialer.interfaces.PrivacyListListener;
import com.nanotricks.stockdialer.models.ContactListModel;

import java.util.List;

public class PrivacyListAdapter extends RecyclerView.Adapter<PrivacyListAdapter.ContactViewHolder> {
    private List<ContactListModel> contactList;
    private Context mContext;
    private String TAG = "ContactListAdapter";
    PrivacyListListener lisenter;

    public PrivacyListAdapter(List<ContactListModel> contactList, Context mContext,PrivacyListListener lisenter) {
        this.contactList = contactList;
        this.mContext = mContext;
        this.lisenter = lisenter;
    }

    @Override
    public PrivacyListAdapter.ContactViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.itemview_privacylist, viewGroup,false);
        return new PrivacyListAdapter.ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PrivacyListAdapter.ContactViewHolder contactViewHolder, int i) {
        final ContactListModel contact = contactList.get(i);
        Log.i(TAG, "onBindViewHolder: "+contact);
        contactViewHolder.name.setText(contact.getName());
        contactViewHolder.phone.setText(contact.getPhoneNumber1());
        contactViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lisenter!=null) {
                    contactViewHolder.contactCheck.setChecked(!contactViewHolder.contactCheck.isChecked());
                    lisenter.onContactChecked(contact.getPhoneNumber1());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder{

        CheckBox contactCheck;
        TextView name;
        TextView phone;

        public ContactViewHolder(View itemView) {
            super(itemView);
            contactCheck = itemView.findViewById(R.id.contact_image_iv);
            name = itemView.findViewById(R.id.privacy_list_name);
            phone = itemView.findViewById(R.id.privacy_list_phone);
        }
    }
}

