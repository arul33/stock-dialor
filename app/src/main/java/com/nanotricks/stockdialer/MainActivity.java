package com.nanotricks.stockdialer;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.nanotricks.stockdialer.dialog.CustomDialogFragment;
import com.nanotricks.stockdialer.fragments.ContactListFragment;
import com.nanotricks.stockdialer.fragments.LoginFragment;
import com.nanotricks.stockdialer.fragments.SplashFragment;
import com.nanotricks.stockdialer.interfaces.OnFragmentInteractionListener;
import com.nanotricks.stockdialer.models.CustomDialogOptions;
import com.nanotricks.stockdialer.models.FragmentInteraction;
import com.nanotricks.stockdialer.utils.Pref;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener {
    private static final int REQUEST_READ_CONTACTS = 01;
    private String TAG = "MainActivity";
    private Context mContext;
    private Pref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_main);
        onBind();
        onDoOperations();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void onDoOperations() {
        replaceFragment(this,SplashFragment.newInstance(),"splashFragment",false,true);
//        replaceFragment(mContext,ContactListFragment.newInstance(),"ContactList",false,true);
    }

    private void onBind() {
        pref = Util.getInstance().getPreference(getApplicationContext());
    }

    public synchronized void replaceFragment(Context context, Fragment fragment, String fragmentTag, Boolean onBackPressed, boolean animation) {
        try {
            FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.root_frame_layout, fragment, fragmentTag);

            if (onBackPressed) {
                transaction.addToBackStack(fragmentTag);
            } else {
                transaction.disallowAddToBackStack();
            }
            if (animation) {
                transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out);
            }
            transaction.commitAllowingStateLoss();
        } catch (Exception e) {
            Log.e(TAG, "onReplaceFragment", e);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onFragmentInteraction(FragmentInteraction data) {
        String action  = data.getAction();
        JSONObject dataIntent  = data.getData();
        switch (action) {
            case "splash":
                checkPermissions();
                checkLogin();
                break;
            case "login":
                try {
                    if(dataIntent.getBoolean("isLogged")) {
                        pref.setBoolean("isLoggedIn",true);
                        pref.setString("name",dataIntent.getString("name"));
                        pref.setString("phone1",dataIntent.getString("phone1"));
                        pref.setString("phone2",dataIntent.getString("phone2"));
                        replaceFragment(mContext,new ContactListFragment(),"Contact",false,true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case "contactList":
                break;
            default:

                break;
        }
    }

    private void checkLogin() {
        Log.i(TAG, "checkLogin");
        if(pref.getBoolean("isLoggedIn")) {
            replaceFragment(mContext,ContactListFragment.newInstance(),"ContactList",false,true);
        } else {
            replaceFragment(mContext,LoginFragment.newInstance(),"Login",false,true);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            this.requestPermissions(new String[]{android.Manifest.permission.READ_CONTACTS},
                    REQUEST_READ_CONTACTS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_CONTACTS: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    CustomDialogOptions options = new CustomDialogOptions("Permission mandatory","App can't continue without needed permissions");
                    options.setNegativeButtonText("Exit :(");
                    options.setNegativeButtonText("Retry");
                    CustomDialogFragment.getInstance(options, new CustomDialogFragment.OnDialogFragmentClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onDialogOkClicked() {
                            checkPermissions();
                        }

                        @Override
                        public void onDialogCancelClicked() {
                            System.exit(0);
                        }
                    });
                    // permission denied,Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }
}
