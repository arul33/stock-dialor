package com.nanotricks.stockdialer.models;

import android.view.Gravity;

public class CustomDialogOptions {
    private String title = "Message";
    private String message;
    private boolean showNegativeButton = true;
    private boolean showPositiveButton = true;
    private String negativeButtonText = "Cancel";
    private String positiveButtonText = "OK";
    private int gravity = Gravity.NO_GRAVITY;

    public CustomDialogOptions() {

    }

    public CustomDialogOptions(String title) {
        this.title = title;
    }

    public CustomDialogOptions(String title, String message) {
        this.title = title;
        this.message = message;
    }

    public CustomDialogOptions(String title, String message, boolean showNegativeButton) {
        this.title = title;
        this.message = message;
        this.showNegativeButton = showNegativeButton;
    }

    public boolean isShowPositiveButton() {
        return showPositiveButton;
    }

    public void setShowPositiveButton(boolean showPositiveButton) {
        this.showPositiveButton = showPositiveButton;
    }

    public CustomDialogOptions(boolean negativeButton) {
        this.showNegativeButton = negativeButton;
    }

    public CustomDialogOptions(boolean negativeButton, String positiveButtonText) {
        this.showNegativeButton = negativeButton;
        this.positiveButtonText = positiveButtonText;
    }

    public CustomDialogOptions(boolean negativeButton, String positiveButtonText, String negativeButtonText) {
        this.showNegativeButton = negativeButton;
        this.positiveButtonText = positiveButtonText;
        this.negativeButtonText = negativeButtonText;
    }

    public boolean isShowNegativeButton() {
        return showNegativeButton;
    }

    public void setShowNegativeButton(boolean showNegativeButton) {
        this.showNegativeButton = showNegativeButton;
    }

    public String getNegativeButtonText() {
        return negativeButtonText;
    }

    public void setNegativeButtonText(String negativeButtonText) {
        this.negativeButtonText = negativeButtonText;
    }

    public String getPositiveButtonText() {
        return positiveButtonText;
    }

    public void setPositiveButtonText(String positiveButtonText) {
        this.positiveButtonText = positiveButtonText;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getGravity() {
        return gravity;
    }

    public void setGravity(int gravity) {
        this.gravity = gravity;
    }
}

