package com.nanotricks.stockdialer.models;

public class ContactListModel {
    private String name;
    private String phoneNumber1;
    private String lastLog;
    private String lastLogType;
    private String contactImage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public String getLastLog() {
        return lastLog;
    }

    public void setLastLog(String lastLog) {
        this.lastLog = lastLog;
    }

    public String getLastLogType() {
        return lastLogType;
    }

    public void setLastLogType(String lastLogType) {
        this.lastLogType = lastLogType;
    }

    public String getContactImage() {
        return contactImage;
    }

    public void setContactImage(String contactImage) {
        this.contactImage = contactImage;
    }
}
