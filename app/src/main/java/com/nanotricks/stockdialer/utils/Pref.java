package com.nanotricks.stockdialer.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Pref {
    private Context context;
    private final SharedPreferences pref;
    private final SharedPreferences.Editor editor;

    public Pref(Context context){
        pref = context.getSharedPreferences("songs",Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public void setString(String preferenceName, String preferenceValue){
        editor.putString(preferenceName,preferenceValue);
        editor.commit();
    }

    public String getString(String preferenceName){
        return (null == pref.getString(preferenceName,null))?"":pref.getString(preferenceName,null);
    }

    public void setInt(String preferenceName, int preferenceValue){
        editor.putInt(preferenceName,preferenceValue);
        editor.commit();
    }

    public int getInt(String preferenceName){
        return pref.getInt(preferenceName,-1);
    }

    public void setBoolean(String preferenceName,Boolean aBoolean){
        editor.putBoolean(preferenceName,aBoolean);
        editor.commit();
    }
    public Boolean getBoolean(String preferenceName){
        return (pref.getBoolean(preferenceName,false));
    }

    public SharedPreferences.Editor getEditor(){
        return  editor;
    }
}
