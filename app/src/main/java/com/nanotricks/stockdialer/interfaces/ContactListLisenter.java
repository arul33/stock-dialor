package com.nanotricks.stockdialer.interfaces;

public interface ContactListLisenter {
    void onContactClick(String number);
    void onContactLongClick(String number);
}
